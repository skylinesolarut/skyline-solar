We are a Full-Service Residential Solar provider, we do not sub out any of our work. We hold all the local licensing required to perform installations. We do all of the design, CAD, permitting, project management, installation and electrical under one roof.

Address: 663 W State St, #3, Pleasant Grove, UT 84062, USA

Phone: 855-475-9765

Website: https://www.skylinesolarpower.com
